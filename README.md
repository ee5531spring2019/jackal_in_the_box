# jackal_in_the_box

Project #2

To create a jackal world with teleop capability.

# Instruction

RUn the following code in your terminal.

1. cd ~/catkin_ws/src
2. git clone https://hli8986@bitbucket.org/ee5531spring2019/jackal_in_the_box.git
3. cd ~/catkin_ws
4. catkin_make
5. source ./devel/setup.bash
6. roslaunch jackal_gazebo jackal_world.launch
7. If you have another jackal_gazebo package, it is suggested that move
that packageout of catkin workspace in order to use this package.

